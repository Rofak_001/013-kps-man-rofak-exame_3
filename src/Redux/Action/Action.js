import Axios from ""
import { GET_POSTS, GET_POST_BY_ID } from "./ActionType"

export const getPosts = () => {
  return async dp => {
    const result = await Axios.get('http://110.74.194.125:3535/api/articles')
    dp({
      type: GET_POSTS,
      data: result.data
    })
  }
}

export const getPostById = (id) => {
  return async dp => {
    const result = await Axios.get(`http://110.74.194.125:3535/api/articles/{id}`)
    console.log(result)
    dp({
      type: GET_POST_BY_ID,
      data: result.data
    })
  }
}